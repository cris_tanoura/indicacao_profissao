package br.com.itau.Profissao.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.Profissao.models.Profissao;

public interface ProfissaoRepository extends CrudRepository<Profissao, Integer> {

		public Optional<Profissao> findByNomeProfissao (String nomeProfissao);
		
		public Profissao getByidProfissao(int id);
		
//		public void saveAll(List<Profissao> profissoes);
}
